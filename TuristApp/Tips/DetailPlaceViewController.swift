//
//  DetailPlaceViewController.swift
//  TuristApp
//
//  Created by Diana Cepeda on 4/21/20.
//  Copyright © 2020 Diana Cepeda. All rights reserved.
//

import UIKit
import GooglePlaces
import Firebase

class DetailPlaceViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelP: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    

    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var vSpinnerDetail : UIView?

    override func viewDidLoad() {
        
        super.viewDidLoad()

        
        descriptionLabel.alpha=0
        labelP.alpha = 0
    resultsViewController = GMSAutocompleteResultsViewController()
    resultsViewController?.delegate = self
    
    let filter = GMSAutocompleteFilter()
        filter.country = "CO"
        resultsViewController?.autocompleteFilter = filter

        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController

        let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: 350.0, height: 45.0))

        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false

           // When UISearchController presents the results view, present it in
           // this view controller, not one further up the chain.
           definesPresentationContext = true
        
    }
}
extension DetailPlaceViewController: GMSAutocompleteResultsViewControllerDelegate{
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        //Handle error
        print("Error: ",error.localizedDescription)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace)
    {
        searchController?.isActive = false
        descriptionLabel.alpha = 1
        labelP.alpha = 1
        showPlacePhoto(place.placeID!)
        descriptionLabel.text = place.name
        
        Analytics.logEvent("search_content_event", parameters: ["search_content": place.name])
        
        labelP.text = "Address:\(place.formattedAddress)"
        
    }
    func showPlacePhoto(_ placeID: String)
    {
        self.showSpinner(onView: self.view)
        let placesClient = GMSPlacesClient()
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.photos.rawValue))!
        
        placesClient.fetchPlace(fromPlaceID: placeID, placeFields: fields, sessionToken: nil, callback: {
            (place: GMSPlace?, error: Error?) in
            if let error = error{
                print("An error occurred: \(error.localizedDescription)")
                return
            }
            if let place = place {
                if place.photos?.count == nil{
                    self.imageView.image = #imageLiteral(resourceName: "bogota5")

                    self.removeSpinner()
                }else {
                let photoMetadata: GMSPlacePhotoMetadata = place.photos! [0]
                
                placesClient.loadPlacePhoto(photoMetadata, callback: { (photo,error) ->Void in
                if let error = error {
                    print("Error loading photo metadata \(error.localizedDescription)")
                    return
                } else {
                    self.imageView.image = photo;

                    self.removeSpinner()
                    }
            })
        }
        }
    })
}
        
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
