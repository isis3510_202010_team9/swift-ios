//
//  ClosePlaceViewController.swift
//  TuristApp
//
//  Created by Diana Cepeda on 26/04/20.
//  Copyright © 2020 Diana Cepeda. All rights reserved.
//

import UIKit
import GooglePlaces

class ClosePlaceViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelP: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var arrayNames: String = ""
    var arrayImagenes: String = ""
    var arrayAdd: String = ""
    
    var vSpinnerClose : UIView?
    
    let group = DispatchGroup() // initialize
    
    var placeTips: GMSPlace?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        switch Network.reachability.status {
        case .unreachable:
        group.enter() // wait
            if arrayNames != "" && arrayAdd != "" && arrayImagenes != "" {
                group.leave()
            }
        group.notify(queue: .main) {
            self.labelP.text = self.arrayNames
            self.descriptionLabel.text = self.arrayAdd
            self.imageView.image = UIImage(named: self.arrayImagenes)
            
            }
        case .wwan:
         group.enter() // wait
                if placeTips != nil {
                    group.leave()
                }
                
                group.notify(queue: .main) {
                    print(self.placeTips)
                    self.labelP.text = self.placeTips?.name
                self.descriptionLabel.text = self.placeTips?.formattedAddress
                self.showPlacePhoto((self.placeTips?.placeID)!)
                }
        case .wifi:
         group.enter() // wait
                if placeTips != nil {
                    group.leave()
                }
                
                group.notify(queue: .main) {
                    print(self.placeTips)
                    self.labelP.text = self.placeTips?.name
                self.descriptionLabel.text = self.placeTips?.formattedAddress
                self.showPlacePhoto((self.placeTips?.placeID)!)
                }
        }
       
       
        
        
    }
    
    func showPlacePhoto(_ placeID: String)
        {
            self.showSpinner(onView: self.view)
            let placesClient = GMSPlacesClient()
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.photos.rawValue))!
            
            placesClient.fetchPlace(fromPlaceID: placeID, placeFields: fields, sessionToken: nil, callback: {
                (place: GMSPlace?, error: Error?) in
                if let error = error{
                    print("An error occurred: \(error.localizedDescription)")
                    return
                }
                if let place = self.placeTips {
                    if place.photos?.count == nil{
                        self.imageView.image = #imageLiteral(resourceName: "bogota5")
                        self.removeSpinner()
                    }else {
                        let photoMetadata: GMSPlacePhotoMetadata = place.photos! [0]
                            placesClient.loadPlacePhoto(photoMetadata, callback: { (photo,error) ->Void in
                            if let error = error {
                                print("Error loading photo metadata \(error.localizedDescription)")
                                return
                            } else {
                                self.imageView.image = photo;
                                self.removeSpinner()
                                }
                        })
                    }
            }
        })
    }

}
